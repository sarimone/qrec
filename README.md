# qRec

qRec app starts recording when it scans the specified QR by the user.  

[![Swift Version][swift-image]][swift-url]
[![License][license-image]][license-url] 
[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

qRec allows users to scan QRs and save the QR strings in a QR picker. qRec will start recording a video once it scans the same selected QR in the QR picker. Once the user presses the stop button the video will be saved in the gallery. Also there is a toggle button for the flash if needed.   

TODO: [![][header.png]]

## Features 

- [x] Scan QR
- [x] Save QR Strings to the QR picker  
- [x] Remove QR strings from the QR picker   
- [x] Video recording 
- [x] Flash mode  

## Requirements 

- iOS 8.0+
- Xcode 9+

## Installation 

1. Clone the repository from GitHub. 
2. Congratulations!  

## References

[appcoda](https://medium.com/appcoda-tutorials/how-to-build-qr-code-scanner-app-in-swift-b5532406dd6b)

## Meta

Sara Bahrini – Sara.bahreini@gmail.com

Simon Rothert - puresamari@gmail.com

Distributed under the [MIT][license-url] license.

[https://github.com/sarabahrini/github-link](https://github.com/dbader/)

[swift-image]:https://img.shields.io/badge/swift-4.2-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
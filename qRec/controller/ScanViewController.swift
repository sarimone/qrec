//
//  QRScannerViewController.swift
//  qRec
//
//  Created by Sara Bahrini on 3/26/19.
//  Copyright © 2019 Sarimone. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Photos



class ScanViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var addQRButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    var captureSession = AVCaptureSession()
    var movieOutput = AVCaptureMovieFileOutput()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var videoCaptureDevice: AVCaptureDevice?
    
    
    @IBOutlet weak var qrPicker: UIPickerView!
    //QR array to store the scanned QRs
    var qrArray = ["NONE","http://www.appcoda.com", "ddd", "KKK"]
    var selectedQR: String = "NONE"
    var scannedQR: String? = nil
    var indexOfPicker = Int()
    
    //let cameraController = CameraController()
    
    
  
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return qrArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return qrArray[row]
    }
    
    func refreshPicker() {
        selectedQR = qrArray[indexOfPicker]
        print(selectedQR)
        addQRButton.isHidden = selectedQR != "NONE"
        removeButton.isHidden = selectedQR == "NONE"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        indexOfPicker = row
        refreshPicker()
    }

    @IBAction func minusButton(_ sender: UIButton) {
        if selectedQR != qrArray[0] {
            qrArray.remove(at:indexOfPicker)
            qrPicker.reloadAllComponents()
            indexOfPicker = qrPicker.selectedRow(inComponent: 0)
            refreshPicker()
        }
        removeButton.isEnabled = true
    }
    
    @IBAction func addQR(_ sender: UIButton) {
        if scannedQR != nil{
            for i in qrArray {
                if i != scannedQR{
                    qrArray.append(scannedQR!)
                    print(qrArray)
                }
            }
            qrPicker.reloadAllComponents()
            
        }
    }
    
    
    @IBAction func flashToggle(_ sender: Any) {
        toggleFlash()
    }
    
    
    @IBAction func stopRecording(_ sender: UIButton) {
        stopRecording()
    }
    
    func toggleFlash(){
        
        guard let device = videoCaptureDevice else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            flashButton.setTitle("Flash " + (device.torchMode == .on ? "Off" : "On"), for: .normal)
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        qrPicker.delegate = self
        qrPicker.dataSource = self
        removeButton.isHidden = true
        stopButton.isHidden = true
    
        
        // Get the back-facing camera for capturing videos
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        videoCaptureDevice = captureDevice
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(captureMetadataOutput)
        self.captureSession.addOutput(self.movieOutput)
        
        // Set delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        captureView.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            captureView.addSubview(qrCodeFrameView)
            captureView.bringSubviewToFront(qrCodeFrameView)
        }
        
    }
    
    func startRecording() {
        if movieOutput.isRecording { return }
        stopButton.isHidden = false
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let fileUrl = paths[0].appendingPathComponent("output.mov")
        try? FileManager.default.removeItem(at: fileUrl)
        movieOutput.startRecording(to: fileUrl, recordingDelegate: self as AVCaptureFileOutputRecordingDelegate)
    }
    
    func stopRecording() {
        if !movieOutput.isRecording { return }
        stopButton.isHidden = true
        movieOutput.stopRecording()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            addQRButton.isEnabled = false
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                addQRButton.isEnabled = true
                scannedQR = metadataObj.stringValue
                if scannedQR == selectedQR { startRecording() }
            }
        }
    }
    
    
    // camera/ video record codes
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ScanViewController:  AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if error == nil {
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(outputFileURL.relativePath) {
                UISaveVideoAtPathToSavedPhotosAlbum(outputFileURL.relativePath, nil, nil, nil)
            }
        }
    }
}
